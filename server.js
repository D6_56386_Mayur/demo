const { response } = require('express')
const express = require('express')
const db = require('./db')
const utils = require('./utils')
const app = express()
app.use(express.json())
app.get('/',(request,response)=>{
    const connection = db.openConnection()
    const statement = `select empid,name,salary,age from EMP`
    connection.query(statement,(error,records)=>{
        
        connection.end()
        response.send(utils.createResult(error,records))
    })
})
app.post('/add',(request,response)=>{
    const {name, salary, age} = request.body
    const connection = db.openConnection()
    const statement = `insert into EMP (name,salary,age) values ('${name}', '${salary}','${age}')`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

app.patch('/update',(request,response)=>{
    const {empid , salary } = request.body
    const statement = `UPDATE EMP set salary = '${salary}' where empid = '${empid}'`
    const connection = db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

app.get('/test',(request,response)=>{
    response.send('hello')
})

app.delete('/delete/:empid',(request,response)=>{
    const {empid} = request.params
    const statement = `DELETE from EMP where empid = '${empid}'`
    const connection = db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})


app.listen(4000,'0.0.0.0',()=>{
    console.log('server listining on 4000')
})
